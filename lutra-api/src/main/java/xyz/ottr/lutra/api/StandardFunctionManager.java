package xyz.ottr.lutra.api;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-api
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import xyz.ottr.lutra.FunctionManager;
import xyz.ottr.lutra.frog.rdffrog.parser.FBaseFunctionParser;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultConsumer;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.io.RDFIO;

@Getter
public class StandardFunctionManager extends FunctionManager {

    private static final String standardFolder = "baseFunctions";
    private static final String baseFunctionFile = "baseFunctions-list.txt";

    public StandardFunctionManager() {
        this.loadFormats();
    }

    private void loadFormats() {
        for (StandardFormat format : StandardFormat.values()) {
            this.registerFormat(format.format);
        }
    }

    public MessageHandler loadBaseFunctions() {
        var standardFunctions = makeDefaultStore(getFormatManager());
        ResultConsumer<AbstractFunction> consumer = new ResultConsumer<AbstractFunction>(standardFunctions);

        var reader = ResultStream.innerFlatMapCompose(RDFIO.inputStreamReader(), new FBaseFunctionParser());

        getLibraryPaths()
                .innerMap(this::getResourceAsStream)
                .innerFlatMap(reader)
                .forEach(consumer);
        super.getFunctionStore().registerBaseFunctions(standardFunctions);
        return consumer.getMessageHandler();
    }

    public ResultStream<String> getLibraryPaths() {

        var functionsList = getResourceAsStream(baseFunctionFile);
        if (functionsList == null) {
            return ResultStream.of(Result.error("Failed to read index file of all functions in the standard library."));
        }

        List<String> paths = new LinkedList<>();

        try {
            paths.addAll(IOUtils.readLines(functionsList, StandardCharsets.UTF_8));
        } catch (IOException ex) {
            return ResultStream.of(Result.error(ex.getMessage()));
        }
        return ResultStream.innerOf(paths);
    }

    private InputStream getResourceAsStream(String path) {
        return this.getClass().getClassLoader().getResourceAsStream(standardFolder + "/" + path);
    }
}
