package xyz.ottr.lutra.store.frogqueries.quries;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.store.frogqueries.FrogCheck;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;

public class CheckGenericVariableExists implements FrogCheck {

    private static final String queryFile = "checkGenericVariableExists.rq";

    @Override
    public String getValidationFile() {
        return queryFile;
    }

    public MessageHandler errorMessage(ResultSet resultSet) {
        var msgs = new MessageHandler();
        var list = ResultSetFormatter.toList(resultSet);
        list.forEach(querySolution -> {
            var functionName = querySolution.get("functionName").toString();
            var genericVariable = Frog.getVarNameFromUniqueId(querySolution.get("genericVariable").asResource());

            msgs.add(Message.error("Generic variable " + genericVariable + " which is used in function "
                    + functionName + " is never declared"));
        });
        return msgs;
    }
}
