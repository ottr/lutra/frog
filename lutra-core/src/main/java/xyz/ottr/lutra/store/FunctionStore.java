package xyz.ottr.lutra.store;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.OTTR;
import xyz.ottr.lutra.io.FormatManager;
import xyz.ottr.lutra.model.Parameter;
import xyz.ottr.lutra.model.Signature;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.terms.FunctionCall;
import xyz.ottr.lutra.store.frogqueries.FrogChecks;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

public class FunctionStore implements Consumer<AbstractFunction> {

    private final Map<String, AbstractFunction> functions;
    private final FormatManager formatManager;
    private FunctionStore baseFunctions;
    private Model model = null;

    private FunctionStore(FormatManager formatManager) {
        this.functions = new HashMap<>();
        this.formatManager = formatManager;
    }

    public FunctionStore(FormatManager formatManager, AbstractFunction... frogFunctions) {
        this(formatManager);
        addFunctions(frogFunctions);
    }

    private void addFunctions(AbstractFunction... frogFunctions) {
        for (AbstractFunction frogFunction : frogFunctions) {
            addFunction(frogFunction);
        }
    }

    private void addFunction(AbstractFunction frogFunction) {
        functions.put(frogFunction.getIdentifier(), frogFunction);
    }

    @Override
    public void accept(AbstractFunction function) {
        addFunction(function);
    }

    public FormatManager getFormatManager() {
        return this.formatManager;
    }

    public void registerBaseFunctions(FunctionStore baseFunctions) {
        this.baseFunctions = baseFunctions;

    }

    public Result<AbstractFunction> getFunction(String iri) {
        var function = getAllFunctions().get(iri);
        return function == null
                ? Result.error(iri + " is not a function")
                : Result.of(function);
    }


    public MessageHandler validateFunctions(PrefixMapping prefixMapping) {
        makeModel(prefixMapping);
        var result = FrogChecks.checkFunctionExist.apply(model);
        result = FrogChecks.checkVariableExists.apply(model).combine(result);
        result = FrogChecks.checkFunctionArg.apply(model).combine(result);
        result = FrogChecks.checkGenericVariableExists.apply(model).combine(result);
        result = FrogChecks.checkUnusedParameters.apply(model).combine(result);
        result = FrogChecks.checkUnusedGenericParameters.apply(model).combine(result);
        if (messageSeverityError(result)) {
            return result;
        }
        result = FrogChecks.checkCntArgs.apply(model).combine(result);
        result = FrogChecks.checkCntArgsGeneric.apply(model).combine(result);
        if (messageSeverityError(result)) {
            return result;
        }
        setFunctionRef();
        return validateArgumentsGenericArgumentsAndReturnType().combine(result);
    }

    private MessageHandler validateArgumentsGenericArgumentsAndReturnType() {
        var mesgs = new MessageHandler();
        functions.forEach((__, function) -> {
            function.validateArguments(mesgs, getAllFunctions());
            function.validateGenericArguments(mesgs);
            function.validateReturnType(mesgs);
        });
        return mesgs;
    }

    public boolean messageSeverityError(MessageHandler messageHandler) {
        return messageHandler.getMostSevere().isGreaterEqualThan(Message.Severity.ERROR);
    }

    private void makeModel(PrefixMapping prefixMapping) {
        if (model != null) {
            return;
        }
        model = ModelFactory.createDefaultModel();
        model.setNsPrefixes(prefixMapping);
        model.setNsPrefixes(OTTR.getDefaultPrefixes());
        if (baseFunctions != null) {
            baseFunctions.addFunctionsToModel(model);
        }
        this.addFunctionsToModel(model);
        //model.write(System.out, "TTL"); //REMOVE
    }

    private void addFunctionsToModel(Model model) {
        functions.forEach((__, function) -> {
            function.toRDF(model, null);
        });
    }

    private void addFunctionCallsToModel(Map<Signature, List<FunctionCall>> functionCallMap) {
        makeModel(null);
        functionCallMap.forEach((sig, functionCallList) -> {
            var sigUniqueId = Frog.createUniqueTemplateIRI(sig.getIri());
            var sigResource = addSignatureToModel(model, sig, sigUniqueId);
            functionCallList.forEach(functionCall -> addFunctionCallToModel(model, sigResource, functionCall, sigUniqueId));
        });
    }

    private Resource addSignatureToModel(Model model, Signature sig, String uniqueSigId) {
        var sigResource = model.createResource(uniqueSigId);
        model.add(model.createStatement(sigResource, Frog.RDFFrog.rdfType, Frog.RDFFrog.Template));
        for (Parameter parameter : sig.getParameters()) {
            var term = parameter.getTerm();
            var type = parameter.getType();
            var parameterResource = model.createResource();
            model.add(model.createStatement(sigResource, Frog.RDFFrog.parameter, parameterResource));
            model.add(model.createStatement(parameterResource, Frog.RDFFrog.parameterType, type.getRDFRepresentation(model, uniqueSigId)));
            model.add(model.createStatement(parameterResource, Frog.RDFFrog.var, term.toRDF(model, uniqueSigId)));
        }
        return sigResource;
    }

    private void addFunctionCallToModel(Model model, Resource sigResource, FunctionCall functionCall, String uniqueSigId) {
        var resource = model.createResource();
        var body = model.createResource();
        model.add(model.createStatement(resource, Frog.RDFFrog.executableFunctionCall, body));
        if (sigResource != null) {
            model.add(model.createStatement(resource, Frog.RDFFrog.usedInTemplate, sigResource));
        }
        functionCall.toRDF(model, body, uniqueSigId);
    }

    private void setFunctionRef() {
        var allFunctions = getAllFunctions();
        functions.forEach((__, functions) -> {
            functions.setFunctions(allFunctions);
        });
    }

    public Map<String, AbstractFunction> getAllFunctions() {
        Map<String, AbstractFunction> allFunctions = new HashMap<>();
        allFunctions.putAll(functions);
        if (baseFunctions != null) {
            allFunctions.putAll(baseFunctions.functions);
        }
        return allFunctions;
    }

    private MessageHandler validateFunctionCallsAndSetFunctionRefs(Map<Signature, List<FunctionCall>> functionCallMap) {
        addFunctionCallsToModel(functionCallMap);
        //model.write(System.out, "TTL"); //REMOVE
        var result = FrogChecks.CHECK_FUNCTION_EXISTS_FC.apply(model);
        if (messageSeverityError(result)) {
            return result;
        }

        result = FrogChecks.checkCntArgsFC.apply(model).combine(result);
        result = FrogChecks.checkCntArgsGenericFC.apply(model).combine(result);
        if (messageSeverityError(result)) {
            return result;
        }

        result = FrogChecks.checkFunctionArgExistsFC.apply(model).combine(result);
        if (messageSeverityError(result)) {
            return result;
        }

        var allFunctions = getAllFunctions();

        MessageHandler finalResult = result;
        functionCallMap.forEach((signature, functionCallList) -> {
            var parameterTerms = signature.getParameters().stream()
                            .map(Parameter::getTerm)
                            .collect(Collectors.toList());
            functionCallList.forEach(functionCall -> {
                functionCall.flatMapSetFunction(allFunctions, parameterTerms, false);
                functionCall.validateGenerics(finalResult, null);
                functionCall.validateArguments(finalResult, null);
            });
        });
        return finalResult;
    }

    public MessageHandler checkFunctionCallsAndSetFunctionRefs(Map<String, Signature> templates) {
        var functionCallMap = templates.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getValue, entry -> entry.getValue().getFunctionCallsInInstances()));
        return validateFunctionCallsAndSetFunctionRefs(functionCallMap);
    }


}
