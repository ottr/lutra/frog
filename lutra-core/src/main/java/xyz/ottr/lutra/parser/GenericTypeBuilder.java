package xyz.ottr.lutra.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import lombok.Builder;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;

public enum GenericTypeBuilder {
    ;

    @Builder

    public static Result<GenericType> createGenericType(Result<Term> term, Result<Type> optionalType) {

        term = Result.nullToEmpty(term, Message.error("Missing generic type terms."));
        optionalType = Result.nullToEmpty(optionalType);
        var builder = Result.of(GenericType.builder());
        builder.addResult(term, GenericType.GenericTypeBuilder::term);
        builder.addResult(optionalType, GenericType.GenericTypeBuilder::optionalType);

        if (Result.allIsPresent(term)) {
            return builder.map(GenericType.GenericTypeBuilder::build)
                    .flatMap(GenericType::validate);
        } else {
            return Result.empty(builder);
        }
    }
}
