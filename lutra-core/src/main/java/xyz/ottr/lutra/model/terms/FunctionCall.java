package xyz.ottr.lutra.model.terms;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.Substitution;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.functions.AbstractFunction;
import xyz.ottr.lutra.model.types.FunctionType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

@Getter
public class FunctionCall implements Term {

    private Term functionName;
    private List<Term> arguments;
    private AbstractFunction function;
    private FunctionType functionType;
    private List<GenericType> genericTypes;

    @Builder(toBuilder = true)
    public FunctionCall(Term functionName, @Singular List<Term> arguments, List<GenericType> genericTypes) {
        this.functionName = functionName;
        this.arguments = arguments;
        this.genericTypes = genericTypes != null ? genericTypes : List.of();
    }

    @Builder
    public static FunctionCall create(Term functionName, List<Term> arguments, List<GenericType> genericTypes) {
        return new FunctionCall(functionName, arguments, genericTypes);
    }

    public void setFunction(AbstractFunction function) {
        this.function = function;
    }

    @Override
    public Term apply(Substitution substitution) {
        var newFunction = this.shallowClone();
        newFunction.arguments = substitution.apply(this.arguments);
        if (isVariable()) {
            newFunction.functionName = substitution.get(functionName);
        }
        return newFunction;
    }

    @Override
    public Object getIdentifier() {
        return functionName;
    }


    public void setVariable(boolean variable) {
        //NOTHING
    }

    @Override
    public boolean isVariable() {
        return functionName.isVariable();
    }

    @Override
    public void setType(Type term) {

    }

    @Override
    public String toString() {
        return functionName.getIdentifier().toString();
    }

    @Override
    public String toString(PrefixMapping prefixMapping) {
        return functionName.getIdentifier().toString()
                + arguments.stream()
                .map(arg -> arg.toString(prefixMapping)).collect(Collectors.toList());
    }

    @Override
    public Type getType() {
        if (function != null) {
            return function.getReturnTypeWithReplacedGenerics(getGenericTypes());
        }
        if (functionType != null) {
            return functionType.getReturnType();
        }
        if (functionName instanceof AbstractFunction || functionName instanceof FunctionTerm) {
            return functionName.getType();
        }
        return null;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof FunctionCall)) {
            return false;
        }
        var otherFunction = (FunctionCall) other;
        if (isVariable() || otherFunction.isVariable()) {
            return false;
        }

        if (!functionName.equals(otherFunction.functionName)) {
            return false;
        }
        if (arguments.size() != otherFunction.arguments.size()) {
            return false;
        }
        for (int i = 0; i < arguments.size(); i++) {
            if (arguments.get(i).isVariable() || otherFunction.arguments.get(i).isVariable()) {
                return false;
            }
            if (!arguments.get(i).equals(otherFunction.arguments.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() { //TODO
        return functionName.hashCode();
    }

    @Override
    public Type getVariableType() {
        return Term.super.getVariableType();
    }

    @Override
    public Optional<Term> unify(Term other) {
        return Optional.empty();
    }

    @Override
    public FunctionCall shallowClone() {
        var functionCall = toBuilder().build();
        functionCall.setFunction(this.function);
        functionCall.arguments = new ArrayList<>(this.arguments);
        functionCall.genericTypes = new ArrayList<>(this.genericTypes);
        return functionCall;
    }

    public void setFunctionType(FunctionType type) {
        functionType = type;
    }

    public List<GenericType> getGenericTypes() {
        return functionName instanceof FunctionTerm
                ? ((FunctionTerm) functionName).getGenericArguments()
                : genericTypes;
    }

    public void toRDF(Model model, Resource blankNode, String identifier) {
        model.add(model.createStatement(blankNode, Frog.RDFFrog.of, functionName.toRDF(model, identifier)));
        model.add(model.createStatement(blankNode, Frog.RDFFrog.rdfType, Frog.RDFFrog.FunctionCall));
        argsToRDF(model, blankNode, identifier);
        genericsToRDF(model, blankNode, identifier, genericTypes);
    }

    public Resource toRDF(Model model, String identifier) {
        var functionCallBlankNode = model.createResource();
        model.add(model.createStatement(functionCallBlankNode, Frog.RDFFrog.rdfType, Frog.RDFFrog.FunctionCall));
        model.add(model.createStatement(functionCallBlankNode, Frog.RDFFrog.of, functionName.toRDF(model, identifier)));
        argsToRDF(model, functionCallBlankNode, identifier);
        genericsToRDF(model, functionCallBlankNode, identifier, genericTypes);
        return functionCallBlankNode;
    }

    private void argsToRDF(Model model, Resource blankNode, String identifier) {
        int cnt = 0;
        for (Term arg : arguments) {
            Resource argBlankNode = model.createResource();
            if (arg instanceof FunctionCall) {
                argBlankNode = model.createResource();
            }
            Literal index = model.createTypedLiteral(cnt++);
            model.add(model.createStatement(blankNode, Frog.RDFFrog.arg, argBlankNode));
            model.add(model.createLiteralStatement(argBlankNode, Frog.RDFFrog.index, index));
            if (arg instanceof LiteralTerm) {
                var lit = (LiteralTerm) arg;
                model.add(model.createLiteralStatement(argBlankNode, Frog.RDFFrog.val, lit.getLiteralToRdf(model)));
            } else {
                model.add(model.createStatement(argBlankNode, Frog.RDFFrog.val, arg.toRDF(model, identifier)));
            }
            if (!(arg instanceof FunctionCall) && !(arg instanceof BlankNodeTerm)) {
                model.add(model.createStatement(argBlankNode, Frog.RDFFrog.argumentType,
                        arg.getType().getRDFRepresentation(model, identifier)));
            }
        }
    }

    public static void genericsToRDF(Model model, Resource blankNode, String identifier, List<GenericType> genericTypes) {
        for (int i = 0; i < genericTypes.size(); i++) {
            var genericRes = genericTypes.get(i).getRDFRepresentation(model, identifier);
            model.add(model.createStatement(blankNode, Frog.RDFFrog.typeArg, genericRes));
            model.add(model.createLiteralStatement(genericRes, Frog.RDFFrog.index, i));
        }
    }

    public String getFunctionCallKey() {
        return functionName.getIdentifier().toString();
    }

    public void flatMapSetFunction(Map<String, AbstractFunction> functions, List<Term> parameterTerms, boolean setVariables) {
        if (!isVariable()) {
            setFunction(functions.get(getFunctionCallKey()));
        } else {
            this.functionType = findTypeOfVariableFunction(parameterTerms);
        }
        var parameterTypes = isVariable()
                ? functionType.getParameterTypes()
                : ((FunctionType) this.function.getType()).getParameterTypes();
        arguments = IntStream.range(0, arguments.size())
                .mapToObj(index -> {
                    var arg = arguments.get(index);
                    var para = parameterTypes.get(index);
                    arg = arg.termWithFunctionRef(functions,para);
                    if (arg instanceof FunctionCall) {
                        ((FunctionCall) arg).flatMapSetFunction(functions, parameterTerms, setVariables);
                    }
                    if (setVariables && arg.isVariable()) {
                        setVariableType(function, arg);
                    }
                    return arg;
                }).collect(Collectors.toList());
    }

    private void setVariableType(AbstractFunction function, Term arg) {
        var type = function.getParameterList().stream()
                .filter(parameter -> parameter.getTerm().equals(arg))
                .findFirst();
        type.ifPresent(parameter -> arg.setType(parameter.getTerm().getType()));

    }

    private FunctionType findTypeOfVariableFunction(List<Term> termList) {
        var functionType = termList.stream()
                .filter(term -> term.equals(functionName))
                .map(Term::getType)
                .filter(type -> type instanceof FunctionType)
                .map(type -> (FunctionType) type)
                .findFirst()
                .orElse(null);
        this.functionType = functionType;
        return functionType;
    }

    //VALIDATION ARGUMENTS
    public void validateArguments(MessageHandler msgs, AbstractFunction headFunction) {
        var parameterTypes = !isVariable()
                ? function.getParametersWithReplacedGenerics(genericTypes)
                : functionType.getParameterTypes();

        for (int i = 0; i < parameterTypes.size(); i++) {
            var paramType = parameterTypes.get(i);
            var arg = arguments.get(i);
            var argType = arg.getType();

            if (!argType.isSubTypeOf(paramType)) {
                var err = isVariable()
                        ? makeErrorMessageArgumentVariable(headFunction, paramType, i, arg, argType)
                        : makeErrorMessageArgument(headFunction, paramType, i, arg, argType);
                msgs.add(new Message(Message.Severity.ERROR, err));
            }
        }
        arguments.stream()
                .filter(arg -> arg instanceof FunctionCall)
                .map(arg -> (FunctionCall) arg)
                .forEach(fc -> fc.validateArguments(msgs, headFunction));
    }

    private String makeErrorMessageArgument(AbstractFunction function, Type paramType, int index, Term arg, Type argType) {
        return "Argument type error in function " + function.getIri().getIdentifier()
                + " . Argument " + arg + " (index " + index + ")"
                + " in function call " + getFunctionName().getIdentifier() + " has type "
                + argType + ", but " + getFunctionName().getIdentifier()
                + " expects the type to be a subtype of a term that has type " + paramType + ".";
    }

    private String makeErrorMessageArgumentVariable(AbstractFunction function, Type paramType, int index, Term arg, Type argType) {
        return "Argument type error in function " + function.getIri().getIdentifier()
                + " . Argument " + arg + " (index " + index + ") "
                + "in the function call with the parameter name " + getFunctionName().getIdentifier()
                + " has type " + argType + ", but " + getFunctionName().getIdentifier()
                + " expects the type to be a subtype of " + paramType + ".";
    }

    @Override
    public void validateGenerics(MessageHandler msgs, Map<Term, Generic> genericMap) {
        if (!isVariable()) {
            function.validateGenerics(msgs, genericTypes, genericMap);
        }
        arguments.forEach(term -> term.validateGenerics(msgs, genericMap));
    }

    //EXECUTION OF FUNCTION CALL
    public Result<Term> execute() {
        return execute(List.of(), List.of()).get();
        /*if(result.isEmpty()){
            var nameResult = Result.of(functionName);
            nameResult.addMessages(result.getAllMessages());
            return nameResult;
        }
        return result;*/
    }

    @Override
    public Supplier<Result<Term>> execute(List<Term> arguments, List<GenericType> genericArguments) {
        var function = getFunction();
        if (function.isEmpty()) {
            return () -> function;
        }
        return function.get().execute(this.arguments, genericTypes);
    }

    private Result<Term> getFunction() {
        if (functionName instanceof FunctionTerm) {
            return Result.of(functionName);
        }
        if (function != null) {
            return Result.of(function);
        }
        return functionName instanceof FunctionCall
                ? functionName.execute(List.of(), List.of()).get()
                : Result.error("Execution error: Cannot find function ");
    }

    @Override
    public Term substitute(Map<Term, Term> parToArg, Map<Term, Type> genericMap) {
        var sub = (FunctionCall) this.shallowClone();
        if (isVariable()) {
            var value = parToArg.get(functionName);
            if (value instanceof AbstractFunction) {
                sub.function = (AbstractFunction) value;
                sub.functionName = sub.function.getIri();
            } else {
                sub.functionName = value;
            }

        }
        sub.genericTypes = sub.genericTypes.stream()
                .map(type -> GenericType.makeGenericTypeFromType(type.replaceGeneric(genericMap)))
                .collect(Collectors.toList());
        sub.arguments = sub.arguments.stream()
                .map(arg -> arg.substitute(parToArg, genericMap))
                .collect(Collectors.toList());
        return sub;
    }
}

