package xyz.ottr.lutra.model;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import lombok.Builder;
import xyz.ottr.lutra.model.terms.FunctionTerm;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;

public enum FunctionTermBuilder {
    ;

    @Builder
    public static Result<FunctionTerm> create(Result<String> iri, Result<List<GenericType>> genericArguments) {
        iri = Result.nullToEmpty(iri, Message.error("Missing IRI for function term"));
        genericArguments = Result.nullToEmpty(genericArguments, Message.error("Missing generic arguments for function term"));

        Result<FunctionTerm.FunctionTermBuilder> builder = Result.of(FunctionTerm.builder());
        builder.addResult(iri, FunctionTerm.FunctionTermBuilder::iri);
        builder.addResult(genericArguments, FunctionTerm.FunctionTermBuilder::genericArguments);

        if (Result.allIsPresent(iri, genericArguments)) {
            return builder.map(FunctionTerm.FunctionTermBuilder::build);
        }
        return Result.empty(builder);
    }
}
