package xyz.ottr.lutra.model.terms;

/*-
 * #%L
 * lutra-core
 * %%
 * Copyright (C) 2018 - 2019 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;
import lombok.Getter;
import lombok.NonNull;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.RDFTurtle;
import xyz.ottr.lutra.model.types.BasicType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.LUBType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.model.types.TypeRegistry;
import xyz.ottr.lutra.system.Result;

@Getter
public class LiteralTerm extends AbstractTerm<String> {

    private final @NonNull String value;
    private final @NonNull String datatype;
    private final String languageTag;

    private LiteralTerm(String value, String datatype, String languageTag) {
        super(getIdentifier(value, datatype, languageTag), getIntrinsicType(datatype));
        this.value = value;
        this.datatype = datatype;
        this.languageTag = languageTag;
    }

    private static String getIdentifier(String value, String datatype, String languageTag) {
        return RDFTurtle.literal(value)
                + (Objects.nonNull(languageTag)
                ? RDFTurtle.literalLangSep + languageTag
                : RDFTurtle.literalTypeSep + datatype);
    }

    private static Type getIntrinsicType(String datatype) {
        return Objects.requireNonNullElse(TypeRegistry.asType(datatype), TypeRegistry.LITERAL);
    }

    public static LiteralTerm createLanguageTagLiteral(String value, @NonNull String languageTag) {
        return new LiteralTerm(value, RDFTurtle.langStringDatatype, languageTag);
    }

    public static LiteralTerm createTypedLiteral(String value, String datatype) {
        return new LiteralTerm(value, datatype, null);
    }

    public static LiteralTerm createPlainLiteral(String value) {
        return new LiteralTerm(value, RDFTurtle.plainLiteralDatatype, null);
    }

    public boolean isLanguageTagged() {
        return Objects.nonNull(this.languageTag);
    }

    public boolean isPlainLiteral() {
        return RDFTurtle.plainLiteralDatatype.equals(this.datatype);
    }

    @Override
    public LiteralTerm shallowClone() {
        LiteralTerm term = new LiteralTerm(this.value, this.datatype, this.languageTag);
        term.setVariable(isVariable());
        return term;
    }

    @Override
    public Optional<Term> unify(Term other) {

        if (!(other instanceof LiteralTerm)) {
            return Optional.empty();
        }
        if (isVariable() || !other.isVariable() && equals(other)) {
            return Optional.of(other);
        }
        return Optional.empty();
    }

    @Override
    public Resource toRDF(Model model, String identifier) {
        return getLiteralToRdf(model).asResource();
    }

    public Literal getLiteralToRdf(Model model) {
        if (getLanguageTag() != null) {
            String tag = getLanguageTag();
            return model.createLiteral(value, tag);
        } else {
            String type = getDatatype();
            TypeMapper tm = TypeMapper.getInstance();
            return model.createTypedLiteral(value, tm.getSafeTypeByName(type));
        }
    }

    @Override
    public Result<Term> termWithRightType(Type type) {
        String dataType;
        if (type instanceof BasicType) {
            dataType = ((BasicType) type).getIri();
        } else if (type instanceof LUBType) {
            var lubInner = ((LUBType) type).getInner();
            if (lubInner instanceof BasicType) {
                dataType = ((BasicType) lubInner).getIri();
            } else if (lubInner instanceof GenericType) {
                dataType = ((GenericType) lubInner).getOptionalType().toString();
            } else {
                return Result.error("A lub type can either have a basic type or a generic type");
            }
        } else {
            return Result.error("A basic type can either have a Basic type og a LUB type");
        }
        return languageTag != null
                ? Result.of(LiteralTerm.createTypedLiteral(value, languageTag))
                : Result.of(LiteralTerm.createTypedLiteral(value, dataType));
    }

}
