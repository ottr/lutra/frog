package xyz.ottr.lutra.model.frog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import lombok.Builder;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.functions.FrogFunction;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;

public enum FunctionBuilder {
    ;

    @Builder
    public static Result<FrogFunction> create(Result<Term> iri, Result<List<Parameter>> parameters, Result<Type> returnType,
                                              Result<Term> functionCall, Result<List<Generic>> genericList) {
        iri = Result.nullToEmpty(iri, Message.error("Missing an name (iri), every Frog function must have a name"));
        parameters = Result.nullToEmpty(parameters, Message.error("Missing parameter list. A Frog.Frog function must have"
                + "a (possibly empty) list of parameter"));
        returnType = Result.nullToEmpty(returnType, Message.error("Missing return type"));
        functionCall = Result.nullToEmpty(functionCall, Message.error("An function needs a function body"));
        genericList = Result.nullToEmpty(genericList);

        Result<FrogFunction.FrogFunctionBuilder> builder = Result.of(FrogFunction.builder());
        builder.addResult(iri, FrogFunction.FrogFunctionBuilder::iri);
        builder.addResult(parameters, FrogFunction.FrogFunctionBuilder::parameterList);
        builder.addResult(returnType, FrogFunction.FrogFunctionBuilder::returnType);
        builder.addResult(functionCall, FrogFunction.FrogFunctionBuilder::functionCall);
        builder.addResult(genericList, FrogFunction.FrogFunctionBuilder::genericList);

        if (Result.allIsPresent(iri, returnType, parameters, functionCall)) {
            return builder.map(FrogFunction.FrogFunctionBuilder::build)
                    .flatMap(FrogFunction::validate)
                    .map(f -> (FrogFunction) f);
        }
        return Result.empty(builder);
    }
}
