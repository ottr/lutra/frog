package xyz.ottr.lutra.model.types;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.terms.BlankNodeTerm;
import xyz.ottr.lutra.model.terms.IRITerm;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.system.Result;




@Getter
public class GenericType implements Type {

    private final Term identifier;
    private final Type optionalType;

    public GenericType(Term term, Type optionalType) {
        this.identifier = term;
        this.optionalType = optionalType;
    }

    public GenericType(Type optionalType) {
        this.identifier = new IRITerm(Frog.TypeURI.nonVariableGeneric);
        this.optionalType = optionalType;
    }

    @Builder
    public static GenericType create(Term term, Type optionalType) {
        return new GenericType(term, optionalType);
    }

    public Result<GenericType> validate() {
        var result = Result.of(this);
        if (!(identifier instanceof BlankNodeTerm) && !(identifier instanceof IRITerm)) {
            result.addError("The generic variable must be a blank node or an iri");
        }
        return result;
    }

    @Override
    public boolean isSubTypeOf(Type other) {
        if (other.equals(TypeRegistry.TOP)) {
            return true;
        }
        if (optionalType != null) {
            return optionalType.isSubTypeOf(other);
        }
        return this.equals(other);
    }

    @Override
    public boolean isCompatibleWith(Type other) {
        return isSubTypeOf(other);
    }

    @Override
    public Type replaceGeneric(Map<Term, Type> genericToType) {
        if (hasOptionalType()) {
            return optionalType.replaceGeneric(genericToType);
        }
        return genericToType.get(identifier);
    }

    @Override
    public Type removeLUB() {
        return null;
    }

    @Override
    public Resource getRDFRepresentation(Model model, String identifier) {
        var res = model.createResource();
        if (optionalType == null) {
            model.add(model.createStatement(res, Frog.RDFFrog.rdfType, Frog.RDFFrog.GenericType));
            model.add(model.createStatement(res, Frog.RDFFrog.type, this.identifier.toRDF(model, identifier)));
        } else {
            model.add(model.createStatement(res, Frog.RDFFrog.type, optionalType.getRDFRepresentation(model, identifier)));
        }

        return res;
    }

    @Override
    public String toString() {
        if (optionalType == null) {
            return "?" + this.getIdentifier().getIdentifier().toString();
        }
        return optionalType.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof GenericType)) {
            return false;
        }
        if (optionalType == null) {
            if (((GenericType) other).optionalType != null) {
                return false;
            }
            return identifier.equals(((GenericType) other).getIdentifier());
        }
        return identifier.equals(((GenericType) other).getIdentifier()) && optionalType.equals(((GenericType) other).getOptionalType());
    }

    @Override
    public int hashCode() {
        if (optionalType == null) {
            return identifier.hashCode();
        }
        return identifier.hashCode() + optionalType.hashCode();
    }

    public boolean hasOptionalType() {
        return optionalType != null;
    }

    public Type getType() {
        return hasOptionalType() ? optionalType : this;
    }

    public static GenericType makeGenericTypeFromType(Type type) {
        if (type instanceof GenericType) {
            return (GenericType) type;
        }
        return new GenericType(type);
    }


}
