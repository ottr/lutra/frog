package xyz.ottr.lutra.model.types;

/*-
 * #%L
 * lutra-core
 * %%
 * Copyright (C) 2018 - 2019 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;

@Getter
@EqualsAndHashCode
public abstract class ComplexType implements Type, HasDepth {

    protected final Type inner;

    protected ComplexType(Type inner) {
        this.inner = inner;
    }

    /**
     * @return the IRI of the (outer) term.
     */
    public abstract String getOuterIRI();

    public BasicType getInnermost() {
        return this.inner instanceof ComplexType
                ? ((ComplexType) this.inner).getInnermost()
                : (BasicType) this.inner;
    }

    public Type getInnerMostType() {
        if (inner instanceof ComplexType) {
            return ((ComplexType) inner).getInnerMostType();
        }
        return inner;
    }

    @Override
    public Resource getRDFRepresentation(Model model, String identifier) {
        return getDepthRepresentation(model, "0", identifier);
    }

    @Override
    public Resource getDepthRepresentation(Model model, String path, String identifiern) {
        var resource = model.createResource();
        var iriOuter = model.createResource(getOuterIRI());
        model.add(model.createStatement(resource, Frog.RDFFrog.rdfType, iriOuter));
        model.add(model.createLiteralStatement(resource, Frog.RDFFrog.depth, path));

        var newPath = path.length() != 0
                ? path + "|0"
                : path + "0";
        var argRec = nextDepth(model, newPath, inner, identifiern);
        model.add(model.createStatement(resource, Frog.RDFFrog.argType, argRec));
        return resource;
    }
}
