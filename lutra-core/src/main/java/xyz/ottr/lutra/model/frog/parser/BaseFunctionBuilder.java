package xyz.ottr.lutra.model.frog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import lombok.Builder;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.functions.BaseFunction;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;

public enum BaseFunctionBuilder {
    ;

    @Builder
    public static Result<BaseFunction> create(Result<Term> iri, Result<List<Parameter>> parameters,
                                              Result<Type> returnType, Result<Term> rule, Result<List<Generic>> genericList) {
        iri = Result.nullToEmpty(iri, Message.error("Missing an name (iri), every Frog.Frog function must have a name"));
        parameters = Result.nullToEmpty(parameters, Message.error("Missing parameter list. A Frog.Frog function must have"
                + "a (possibly empty) list of parameter"));
        returnType = Result.nullToEmpty(returnType, Message.error("Missing return type"));
        rule = Result.nullToEmpty(rule, Message.error("A base function needs a rule"));


        Result<BaseFunction.BaseFunctionBuilder> builder = Result.of(BaseFunction.builder());
        builder.addResult(iri, BaseFunction.BaseFunctionBuilder::iri);
        builder.addResult(parameters, BaseFunction.BaseFunctionBuilder::parameterList);
        builder.addResult(returnType, BaseFunction.BaseFunctionBuilder::returnType);
        builder.addResult(rule, BaseFunction.BaseFunctionBuilder::rule);
        builder.addResult(genericList, BaseFunction.BaseFunctionBuilder::genericList);


        if (Result.allIsPresent(iri, returnType, parameters)) {
            return builder.map(BaseFunction.BaseFunctionBuilder::build)
                    .flatMap(BaseFunction::validate);
        }
        return Result.empty(builder);
    }
}
