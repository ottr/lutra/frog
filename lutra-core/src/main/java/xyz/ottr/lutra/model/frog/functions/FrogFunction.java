package xyz.ottr.lutra.model.frog.functions;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-core
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.terms.FunctionCall;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.model.types.TypeRegistry;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.MessageHandler;
import xyz.ottr.lutra.system.Result;

@Getter
public class FrogFunction extends AbstractFunction {

    @NonNull
    @Singular
    private FunctionCall functionCall;

    public FrogFunction(Term iri, List<Parameter> parameterList, Type returnType,
                        FunctionCall functionCall, List<Generic> genericList) {
        super(iri, parameterList, returnType, genericList);
        this.functionCall = functionCall;
    }

    @Builder
    public static FrogFunction create(Term iri, List<Parameter> parameterList, Type returnType,
                                      Term functionCall, List<Generic> genericList) {
        if (!(functionCall instanceof FunctionCall)) {
            return null;
        }
        return new FrogFunction(iri, parameterList, returnType, (FunctionCall) functionCall, genericList);
    }


    @Override
    public Term shallowClone() {
        return new FrogFunction(getIri(), getParameterList(), getReturnType(),
                (FunctionCall) getFunctionCall().shallowClone(), getGenericList());
    }

    @Override
    public String toString(PrefixMapping prefixes) {
        return prefixes.shortForm(getIri().toString())
                + getParameterList().stream()
                .map(t -> t.toString(prefixes))
                .collect(Collectors.joining(", ", "[ ", " ]"));
    }

    @Override
    public Resource toRDF(Model model, String identifier) {
        super.toRDF(model, null);
        Resource functionIri = model.createResource(getIri().getIdentifier().toString());
        Resource body = model.createResource();
        model.add(model.createStatement(functionIri, Frog.RDFFrog.body, body));
        functionCall.toRDF(model, body, getIriString());
        return null;
    }

    @Override
    public void setFunctions(Map<String, AbstractFunction> functions) {
        var parameters = getParameterList().stream()
                .map(Parameter::getTerm)
                .collect(Collectors.toList());
        functionCall.flatMapSetFunction(functions, parameters, true);
    }

    //JAVA VALIDATION IMPLEMENTATION
    @Override
    public void validateArguments(MessageHandler msgs, Map<String, AbstractFunction> functions) {
        functionCall.validateArguments(msgs, this);
    }

    @Override
    public void validateGenericArguments(MessageHandler msgs) {
        functionCall.validateGenerics(msgs, getGenericMap());
    }

    @Override
    public void validateReturnType(MessageHandler msgs) {
        var actuallyReturnType = functionCall.getType();
        if (!actuallyReturnType.isSubTypeOf(getReturnType())) {
            var err = "Return type error. Function " + getIri().getIdentifier() + " has return type " + getReturnType()
                    + ", but is returning a " + actuallyReturnType;
            msgs.add(Message.error(err));
        }
    }

    @Override
    public Supplier<Result<Term>> execute(List<Term> arguments, List<GenericType> genericArguments) {
        if (getLookupTable().containsKey(arguments)) {
            // System.out.println("USES LOOKUP TABLE FROG FUNCTION");
            return () -> getResultFromLookupTable(arguments, genericArguments);
        }
        var substitutedFunctionBody = functionCall.substitute(makeParToArgMap(arguments),
                makeGenericMap(genericArguments));
        var execution = substitutedFunctionBody.execute(List.of(), List.of());
        return () -> {
            if (getLookupTable().containsKey(arguments)) {
                //System.out.println("USES LOOKUP TABLE FROG FUNCTION");
                return getResultFromLookupTable(arguments, genericArguments);
            }
            var result = execution.get();
            var returnType = getReturnTypeWithReplacedGenerics(genericArguments);
            if (result.isPresent() && ! result.get().getType().equals(TypeRegistry.BOT)) {
                result.get().setType(returnType);
            }
            addResultToLookupTable(arguments, result, returnType);
            return result;
        };
    }
}
