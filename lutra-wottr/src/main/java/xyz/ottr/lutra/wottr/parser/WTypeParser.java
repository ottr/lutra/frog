package xyz.ottr.lutra.wottr.parser;

/*-
 * #%L
 * lutra-wottr
 * %%
 * Copyright (C) 2018 - 2019 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.OTTR;
import xyz.ottr.lutra.model.types.BasicType;
import xyz.ottr.lutra.model.types.FunctionType;
import xyz.ottr.lutra.model.types.LUBType;
import xyz.ottr.lutra.model.types.ListType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.model.types.TypeRegistry;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.wottr.util.RDFNodes;

public class WTypeParser implements Function<RDFNode, Result<Type>> {

    public Result<Type> apply(RDFNode node) {
        return node.canAs(RDFList.class)
                ? apply(node.as(RDFList.class).asJavaList())
                : node.isAnon()
                    ? Result.error("Error parsing type: A type can't be a blank node")
                    : parseBaseType(node);
    }

    public Result<Type> apply(List<RDFNode> nodes) {
        if (nodes.isEmpty()) {
            return Result.error("Error parsing type. Expected uri(s) denoting a type, but got nothing.");
        }

        var first = toURI(nodes.remove(0));
        return first.flatMap(uri -> {
            switch (uri) {
                case OTTR.TypeURI.List:
                    return parseListType(nodes);
                case OTTR.TypeURI.NEList:
                    return parseNEListType(nodes);
                case OTTR.TypeURI.LUB:
                    return parseLubType(nodes);
                case OTTR.TypeURI.Function:
                    return parseFunctionType(nodes);
                default:
                    return Result.error("Unrecognized type: " + uri + ".");
            }
        });
    }

    protected Result<Type> parseBaseType(RDFNode node) {
        var uri = toURI(node);
        if (uri.isPresent() && OTTR.TypeURI.complexTypes.contains(uri.get())) {
            return Result.error("Error parsing type: " + uri.get() + " is a complex typed, however, it is used as a base type");
        }
        return toURI(node).flatMap(TypeRegistry::get);
    }

    private Result<Type> parseListType(List<RDFNode> nodes) {
        if (nodes.size() == 1 && !OTTR.TypeURI.complexTypes.contains(nodes.get(0).toString())) {
            return apply(nodes.get(0)).map(ListType::new);
        }
        return apply(nodes).map(ListType::new);
    }

    private Result<Type> parseNEListType(List<RDFNode> nodes) {
        if (nodes.size() == 1 && !OTTR.TypeURI.complexTypes.contains(nodes.get(0).toString())) {
            return apply(nodes.get(0)).map(ListType::new);
        }
        return apply(nodes).map(ListType::new);
    }

    protected Result<Type> parseLubType(List<RDFNode> nodes) {
        if (nodes.size() != 1) {
            return Result.error("Error parsing LUB type: only expects one type argument");
        }
        var rest = apply(nodes.get(0));
        if (rest.isPresent() && !(rest.get() instanceof BasicType)) {
            return Result.error("Error parsing LUB type: type argument must be basic type");
        }
        return rest.map(type -> new LUBType((BasicType) type));
    }

    private Result<Type> parseFunctionType(List<RDFNode> nodes) {
        if (nodes.isEmpty()) {
            return Result.error("Error parsing Function type: must have at least one type argument");
        }
        var argTypes = nodes.stream()
                .map(this)
                .collect(Collectors.toList());
        return Result.aggregate(argTypes).map(FunctionType::new);
    }

    protected Result<String> toURI(RDFNode node) {
        return RDFNodes.castURIResource(node)
                .map(Resource::getURI);
    }
}
