package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.parser.ParameterBuilder;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.parser.ModelSelector;
import xyz.ottr.lutra.wottr.parser.WTermParser;


public class FParameterParser {
    private final List<Result<Type>> types;
    private final List<Result<Term>> terms;

    private final Map<String, Term> variables = new HashMap<>();

    FParameterParser(Model model, Resource function, Result<RDFList> defList) {
        this.types = parseParameterTypes(model, function, new FTypeParser());
        this.terms = parseTerms(defList);
    }

    public Result<List<Parameter>> parseParameters() {
        if (types.size() != terms.size()) {
            return Result.error("Needs as many types as variables, but " + terms.size() + " where given, while "
                    + types.size() + " types where given");
        }
        List<Result<Parameter>> parameters = new LinkedList<>();
        for (int i = 0; i < terms.size(); i++) {
            if (terms.get(i).isEmpty()) {
                return Result.empty(terms.get(i));
            }
            if (types.get(i).isEmpty()) {
                return Result.empty(types.get(i));
            }
            variables.put(terms.get(i).get().getIdentifier().toString(), terms.get(i).get());
            parameters.add(
                    ParameterBuilder.builder()
                            .type(types.get(i))
                            .term(terms.get(i))
                            .build()
            );
        }
        return Result.aggregate(parameters);
    }


    private List<Result<Term>> parseTerms(Result<RDFList> defList) {
        return defList
                .map(lst -> lst.get(1))
                .filterOrMessage(node -> node.canAs(RDFList.class), new Message(Message.Severity.ERROR,
                        "The parameter variable must be a list"))
                .map(node -> node.as(RDFList.class))
                .map(RDFList::asJavaList)
                .mapToStream(ResultStream::innerOf)
                .mapFlatMap(WTermParser::toTerm)
                .collect(Collectors.toList());

    }


    private List<Result<Type>> parseParameterTypes(Model model, Resource function, FTypeParser typeParser) {
        return ModelSelector.getRequiredResourceObject(model, function, Frog.RDFFrog.type)
                .flatMap(lt -> ModelSelector.getRequiredListObject(model, lt, Frog.RDFFrog.parameterTypes))
                .map(RDFList::asJavaList)
                .mapToStream(ResultStream::innerOf)
                .mapFlatMap(typeParser::apply)
                .collect(Collectors.toList());
    }
}
