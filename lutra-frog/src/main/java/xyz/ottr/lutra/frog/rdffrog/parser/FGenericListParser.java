package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.parser.GenericBuilder;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.system.Message;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.parser.ModelSelector;
import xyz.ottr.lutra.wottr.parser.WTermParser;
import xyz.ottr.lutra.wottr.parser.WTypeParser;

public class FGenericListParser {
    public static Result<List<Generic>> parseGeneric(Model model, Resource function) {
        var listOfGenerics = ModelSelector.getOptionalListObject(model, function, Frog.RDFFrog.typeVars)
                .map(RDFList::asJavaList)
                .mapToStream(ResultStream::innerOf)
                .mapFlatMap(rdfNode -> parseGeneric(model, rdfNode))
                .collect(Collectors.toList());
        return Result.aggregate(listOfGenerics);
    }

    private static Result<Generic> parseGeneric(Model model, RDFNode generic) {
        if (!generic.isResource() && !generic.isAnon()) {
            return Result.error("A generic parameter is defined by a blank node");
        }
        var varValue = ModelSelector.getRequiredResourceObject(model, generic.asResource(), Frog.RDFFrog.var)
                .filterOrMessage(RDFNode::isAnon, Message.error("A generic parameter must be a blank node"))
                .map(node -> node.asResource().getId().getBlankNodeId())
                .flatMap(WTermParser::toBlankNodeTerm)
                .map(blankNode -> (Term) blankNode);
        var subtypeOf = ModelSelector.getRequiredResourceObject(model, generic.asResource(), Frog.RDFFrog.subtypeOf)
                .flatMap(res -> new WTypeParser().apply(res));
        return GenericBuilder.createGeneric(varValue, subtypeOf);
    }
}

