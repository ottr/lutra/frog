package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.Resource;
import xyz.ottr.lutra.Frog;
import xyz.ottr.lutra.model.frog.Parameter;
import xyz.ottr.lutra.model.frog.parser.ParameterBuilder;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.parser.TermParser;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.system.ResultStream;
import xyz.ottr.lutra.wottr.parser.ModelSelector;

public class FBaseParameterParser {

    private final List<Result<Type>> types;

    FBaseParameterParser(Model model, Resource function) {
        this.types = parseParameterTypes(model, function, new FTypeParser());
    }

    public Result<List<Parameter>> parseParameters() {
        List<Result<Parameter>> parameters = new LinkedList<>();
        for (Result<Type> type : types) {
            parameters.add(
                    ParameterBuilder.builder()
                            .type(type)
                            .term(parseTerms())
                            .build()
            );
        }
        return Result.aggregate(parameters);
    }

    private Result<Term> parseTerms() {
        return TermParser.newBlankNodeTerm()
                .map(blankNodeTerm -> (Term) blankNodeTerm);
    }

    private List<Result<Type>> parseParameterTypes(Model model, Resource function, FTypeParser typeParser) {
        return ModelSelector.getRequiredResourceObject(model, function, Frog.RDFFrog.type)
                .flatMap(lt -> ModelSelector.getRequiredListObject(model, lt, Frog.RDFFrog.parameterTypes))
                .map(RDFList::asJavaList)
                .mapToStream(ResultStream::innerOf)
                .mapFlatMap(typeParser)
                .collect(Collectors.toList());
    }
}
