package xyz.ottr.lutra.frog.rdffrog.parser;

/*-
 * #%L
 * lutra-wottr
 * %%
 * Copyright (C) 2018 - 2019 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import java.util.List;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.BasicType;
import xyz.ottr.lutra.model.types.GenericType;
import xyz.ottr.lutra.model.types.LUBType;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.parser.GenericTypeBuilder;
import xyz.ottr.lutra.system.Result;
import xyz.ottr.lutra.wottr.parser.WTermParser;
import xyz.ottr.lutra.wottr.parser.WTypeParser;

public class FTypeParser extends WTypeParser {

    @Override
    public Result<Type> apply(RDFNode node) {
        return node.canAs(RDFList.class)
                ? apply(node.as(RDFList.class).asJavaList())
                : node.isAnon()
                    ? parseGenericType(node)
                    : parseBaseType(node);
    }

    @Override
    protected Result<Type> parseLubType(List<RDFNode> nodes) {
        if (nodes.size() != 1) {
            return Result.error("Error parsing LUB type: only expects one type argument");
        }
        var rest = apply(nodes.get(0));
        if (rest.isPresent() && !(rest.get() instanceof BasicType) && !(rest.get() instanceof GenericType)) {
            return Result.error("Error parsing LUB type: type argument must be basic type");
        }
        return rest.map(type -> new LUBType((BasicType) type));
    }

    private Result<Type> parseGenericType(RDFNode node) {
        var blankNodeId = node.asResource().getId().getBlankNodeId();
        return GenericTypeBuilder.builder()
                .term(WTermParser.toBlankNodeTerm(blankNodeId).map(t -> (Term) t))
                .build()
                .map(gt -> (Type) gt);
    }
}

