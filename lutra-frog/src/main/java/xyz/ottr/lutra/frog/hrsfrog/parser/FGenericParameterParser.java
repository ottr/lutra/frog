package xyz.ottr.lutra.frog.hrsfrog.parser;

/*-
 * #%L
 * xyz.ottr.lutra:lutra-frog
 * %%
 * Copyright (C) 2018 - 2022 University of Oslo
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-2.1.html>.
 * #L%
 */

import xyz.ottr.lutra.model.frog.Generic;
import xyz.ottr.lutra.model.frog.parser.GenericBuilder;
import xyz.ottr.lutra.model.terms.Term;
import xyz.ottr.lutra.model.types.Type;
import xyz.ottr.lutra.stottr.antlr.stOTTRParser;
import xyz.ottr.lutra.stottr.parser.SBaseParserVisitor;
import xyz.ottr.lutra.stottr.parser.STermParser;
import xyz.ottr.lutra.stottr.parser.STypeParser;
import xyz.ottr.lutra.system.Result;

public class FGenericParameterParser extends SBaseParserVisitor<Generic> {

    private final STypeParser typeParser;
    private final STermParser termParser;

    public FGenericParameterParser(STermParser termParser) {
        this.termParser = termParser;
        this.typeParser = new STypeParser(termParser, false);
    }

    public Result<Generic> visitGeneric(stOTTRParser.GenericParameterContext ctx) {
        return GenericBuilder.builder()
                .term(parseTerm(ctx))
                .type(parseType(ctx))
                .build();
    }

    private Result<Term> parseTerm(stOTTRParser.GenericParameterContext ctx) {
        return termParser.toBlankNodeTerm(termParser.getVariableLabel(ctx.Variable())).map(t -> (Term) t);
    }

    private Result<Type> parseType(stOTTRParser.GenericParameterContext ctx) {
        return ctx.type() != null
                ? this.typeParser.visit(ctx)
                : null;
    }
}
